from flask import Flask, render_template, redirect, url_for, flash, request, session, make_response
from config import *
from passlib.hash import sha256_crypt
from flask_mail import Mail, Message
import pymysql, re, os
from datetime import datetime, date
from functools import wraps, update_wrapper


app = Flask(__name__)
app.secret_key = APP_SECRET_KEY

dbcon = pymysql.connect(host=DB_HOST, user=DB_USERNAME, password=DB_PASSWORD, db=DB_NAME)

app.config['MAIL_SERVER'] = MAIL_SERVER
app.config['MAIL_PORT'] = MAIL_PORT
app.config['MAIL_USERNAME'] = MAIL_USERNAME
app.config['MAIL_PASSWORD'] = MAIL_PASSWORD
app.config['MAIL_USE_TLS'] = True
app.config['MAIL_USE_SSL'] = False
mail = Mail(app)


def nocache(view):
    @wraps(view)
    def no_cache(*args, **kwargs):
        response = make_response(view(*args, **kwargs))
        response.headers['Last-Modified'] = datetime.now()
        response.headers['Cache-Control'] = 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0'
        response.headers['Pragma'] = 'no-cache'
        response.headers['Expires'] = '-1'
        return response

    return update_wrapper(no_cache, view)



@app.route('/')
def home():
    return redirect(url_for('ulogin'))



@app.route('/user/dashboard/')
@nocache
def udashboard():
    if 'loginemail' in session:

        firstname = session.get('firstname')
        lastname = session.get('lastname')
        return render_template('/user/index.html', firstname=firstname, lastname=lastname)

    else:
        flash("You're not logged in. Please login.", 'info')
        return redirect(url_for('ulogin'))



@app.route('/user/register/', methods=['GET', 'POST'])
def uregister():
    try:
        if request.method == 'POST':
            firstname = request.form['user_fname']
            lastname = request.form['user_lname']
            phonenumber = request.form['user_phone']
            emailaddress = request.form['user_email']
            password1 = request.form['user_pass1']
            password2 = request.form['user_pass2']
            paymentcategory = request.form['payment_category']
            mpesaowner = request.form['mpesa_owner']
            mpesanumber = request.form['mpesa_number']

            cur = dbcon.cursor()
            cur.execute('SELECT * FROM members WHERE email = %s', emailaddress)
            member = cur.fetchone()
            cur.close()
            if member:
                flash('A user with the email address exists. Please try again!', 'danger')
                return redirect(url_for('uregister'))
            elif password1 != password2:
                flash('The passwords entered must match. Please try again', 'danger')
                return redirect(url_for('uregister'))
            elif len(password1) < 8:
                flash('Password must be at least 8 characters long.', 'danger')
                return redirect(url_for('uregister'))
            elif not re.search('[a-z]', password1):
                flash('Password must include at least one small letter.', 'danger')
                return redirect(url_for('uregister'))
            elif not re.search('[A-Z]', password1):
                flash('Password must include at least one capital letter.', 'danger')
                return redirect(url_for('uregister'))
            elif not re.search('[0-9]', password1):
                flash('Password must include at least one digit.', 'danger')
                return redirect(url_for('uregister'))
            else:
                otp = os.urandom(12).hex()
                confirmationurl = PROTOCOL + URL + DOC_ROOT + 'completeregistration/'+'?regkey=' +'&otp=' + otp +'&mail=' + emailaddress
                db_password = sha256_crypt.hash(password1)

                cur = dbcon.cursor()
                cur.execute(
                    'INSERT INTO members(firstname, lastname, phone, email, paymentcategory, password, '
                    'mpesaowner, mpesanumber, registration_token) '
                    'VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s)',
                    (firstname, lastname, phonenumber, emailaddress, paymentcategory, db_password, mpesaowner,
                     mpesanumber, otp))
                cur.execute('UPDATE members SET expiry_registration_token = now() + INTERVAL 1 HOUR WHERE email = %s',
                            emailaddress)
                dbcon.commit()
                cur.close()
                dbcon.close()

                msg = Message('OTP', sender=DEFAULT_SENDER, recipients=[emailaddress])
                msg.html = render_template('/user/otpconfirmation.html', firstname=firstname, otp=otp,
                                           confirmationurl=confirmationurl)
                mail.send(msg)

                flash('Success! You have successfully created an account. Check email for further steps.', 'success')
                return redirect(url_for('uregister'))

    except Exception as e:
        return render_template('/user/500.html', error=e)

    return render_template("/user/register.html")



@app.route("/completeregistration/", methods=['GET', 'POST'])
def completereg():
    otp = request.args.get('otp')
    emailaddress = request.args.get('mail')

    cur = dbcon.cursor()
    cur.execute("SELECT * FROM members WHERE registration_token = %s", otp)
    member = cur.fetchone()

    if member:
        cur = dbcon.cursor()
        cur.execute("UPDATE members SET status = %s, expiry_registration_token=now() WHERE registration_token = %s",
                    ('active', otp))
        dbcon.commit()

        msg = Message('Registration Confirmation', sender=DEFAULT_SENDER, recipients=[emailaddress])
        msg.html = render_template('/user/registrationcompletion.html', ORG_NAME=ORG_NAME)
        mail.send(msg)

        cur.close()

        flash('Success! Registration compeleted, you can now login.', 'success')
        return redirect(url_for('ulogin'))

    else:
        flash("The OTP is invalid. Please access portal and resend the validation email.", "danger")
        return redirect(url_for('ulogin'))



@app.route('/user/login/', methods=['GET', 'POST'])
def ulogin():
    try:
        if request.method == 'POST':
            loginemail = request.form['login_email']
            loginpassword = request.form['login_password']

            cur = dbcon.cursor()
            cur.execute('SELECT id,firstname,lastname,password FROM members WHERE email = %s', loginemail)
            member = cur.fetchone()
            (id, firstname, lastname, password) = member #unpack tuple

            #sha256_crypt.verify(txt, hash)
            isPasswordCorrect = sha256_crypt.verify(loginpassword, password)


            if isPasswordCorrect == True:
                #session['loggedin'] = True
                session['loginemail'] = loginemail
                session['id'] = id
                session['firstname'] = firstname
                session['lastname'] = lastname

                flash('Welcome back. You have successfully logged in.', 'success')
                return redirect(url_for('udashboard'))
            else:
                flash('Invalid login. Please try again.', 'danger')
                return redirect(url_for('ulogin'))

    except Exception as e:
        return render_template('/user/500.html', error=e)


    return render_template('/user/login.html')


@app.route('/user/pwreset/', methods=['GET', 'POST'])
def uforgotpass():
    try:
        if request.method == 'POST':
            loginemail = request.form['login_email']

            cur = dbcon.cursor()
            cur.execute('SELECT * FROM members WHERE email = %s', loginemail)
            member = cur.fetchone()
            cur.close()

            if member:
                flash('Please check your inbox (or spam/junk folder) for password reset instructions we have sent.',
                      'info')
                resetkey = os.urandom(12).hex()
                now = datetime.now()
                timenow = now.strftime("%H:%M:%S")
                day = str(date.today())
                finalkey = '?rusetkey=&time=' + timenow + '-&day=' + day + '-&key=' + resetkey + '&mail=' + loginemail
                reseturl = PROTOCOL + URL + DOC_ROOT + 'keycheck/' + finalkey

                cur = dbcon.cursor()
                cur.execute('UPDATE members SET reset_token = %s WHERE email = %s', (resetkey, loginemail))
                cur.execute('UPDATE members SET expiry_reset_token=now() + INTERVAL 30 MINUTE WHERE email = %s',
                            loginemail)
                dbcon.commit()
                cur.close()

                msg = Message('Password Reset Instructions', sender=DEFAULT_SENDER, recipients=[loginemail])
                msg.html = render_template('/user/passwordreset.html', loginemail=loginemail, reseturl=reseturl,
                                           ORG_NAME=ORG_NAME)
                mail.send(msg)
                return redirect(url_for('uforgotpass'))
            else:
                flash('The email address does not exists in our system.', 'warning')
                return redirect(url_for('uforgotpass'))

    except Exception as e:
        return render_template('/user/500.html', error=e)

    return render_template("/user/forgot-password.html")


@app.route("/keycheck/", methods=['GET', 'POST'])
def keycheck():
    resetkey = request.args.get('key')
    loginemail = request.args.get('mail')

    cur = dbcon.cursor()
    cur.execute('SELECT expiry_reset_token FROM members WHERE reset_token = %s', resetkey)
    member = cur.fetchone()
    cur.close()

    if not member:
        flash('The key is invalid. Please repeat the reset procedure.', 'danger')
        return redirect(url_for('ulogin'))
    else:
        session['loginemail'] = loginemail

        if request.method == 'POST':
            password1 = request.form['user_pass1']
            password2 = request.form['user_pass2']

            if password1 != password2:
                flash('The passwords entered must match. Please try again', 'danger')
                return redirect(request.referrer)
            elif len(password1) < 7:
                flash('Password must be at least 7 characters long.', 'danger')
                return redirect(request.referrer)
            elif not re.search('[a-z]', password1):
                flash('Password must include at least one small letter.', 'danger')
                return redirect(request.referrer)
            elif not re.search('[A-Z]', password1):
                flash('Password must include at least one capital letter.', 'danger')
                return redirect(request.referrer)
            elif not re.search('[0-9]', password1):
                flash('Password must include at least one digit.', 'danger')
                return redirect(url_for('changepass'))
            else:
                db_password = sha256_crypt.hash(password1)
                cur = dbcon.cursor()
                cur.execute('UPDATE members SET password = %s WHERE email = %s', (db_password, loginemail))
                dbcon.commit()
                cur.close()
                flash('Your password has been successfully updated.', 'success')
                return redirect(url_for('ulogin'))
    return render_template('/user/newpassword.html')


@app.route('/logout/')
@nocache
def ulogout():
    session.pop('loginemail')
    session.pop('id')
    session.pop('firstname')
    session.pop('lastname')
    return redirect(url_for('ulogin'))


@app.errorhandler(404)
def pagenotfound(e):
    return render_template("/user/404.html")


@app.errorhandler(500)
def servererror(e):
    return render_template("/user/500.html", error=e)


if __name__== "__main__":
    app.run(debug=True, port=5005)